const aws4  = require('aws4');
const axios = require('axios');
const Bottleneck = require("bottleneck");
const config = require("./config.json");

const makeRequest = (settings, nextToken, pathData) => {
    let params = {
        service: 'execute-api',
        region: config.regions[settings.region].awsRegion,
        method: config.methods[settings.type].method,
        path: config.methods[settings.type].path,
        host: config.regions[settings.region].host,
        headers: {
            'x-amz-access-token': settings.accessToken
        },
        body: settings.body || ""
    };
    if(config.methods[settings.type].method === "POST") params.headers["Content-Type"] = "application/json";
    let hasQuery = false;
    if(config.methods[settings.type].directPath) {
        if(params.path[params.path.length - 1] != "/") params.path += "/";
        if(pathData) params.path += pathData;
        if(config.methods[settings.type].pathAfter) params.path += config.methods[settings.type].pathAfter;
    } else {
        if (settings.queryParams) {
        Object.keys(settings.queryParams).forEach((key, index) => {
                if(index === 0) {
                    //if(params.path[params.path.length - 1] != "/") params.path += "/";
                    params.path += "?" + key + "=" + settings.queryParams[key];
                    hasQuery = true;
                } else {
                    params.path += "&" + key + "=" + settings.queryParams[key];
                    hasQuery = true;
                }
            });
        }
        if(nextToken) {
            if(hasQuery) {
                params.path += "&NextToken=" + encodeURIComponent(nextToken);
            } else {
                if(params.path[params.path.length - 1] != "/") params.path += "/";
                params.path += "?NextToken=" + encodeURIComponent(nextToken);
            }
        }
    }
    
    aws4.sign(params, settings.iamUserAuth);
    //console.log("params", params);
    return axios({
        method: config.methods[settings.type].method,
        url: `https://${params.host}${params.path}`,
        headers: params.headers,
        data: params.body
    })
    .then(r => ({
        body: r.data.payload,
        nextToken: r.data.payload && r.data.payload.NextToken
    }))
    .catch(e => {
        if(e.isAxiosError) {
            throw Error(JSON.stringify(e.response.data));
        } else {
            throw Error(e)
        }
    })
}

module.exports = {
    request: async (settings) => {
        // Create Request params
        if(!settings.region || !settings.bottleneckConfig || !settings.accessToken) throw Error("Settings incomplete");
        

        const limiter = new Bottleneck({
            reservoir: config.methods[settings.type].r || 1,
            reservoirIncreaseAmount: config.methods[settings.type].i || 1,
            reservoirIncreaseInterval: 1000,
            reservoirIncreaseMaximum: config.methods[settings.type].r || 1,
            minTime: 500,
            id: "amz" + settings.account + settings.type,
            datastore: "local",
            clearDatastore: true,
            clientOptions: {
                host: settings.bottleneckConfig.host,
                port: settings.bottleneckConfig.port,
                password: settings.bottleneckConfig.password
            }
        });
        limiter.on("failed", async (error, jobInfo) => {
          const id = jobInfo.options.id;
          console.warn(`Job ${id} failed: ${error}`);
         
          if (jobInfo.retryCount === 0) { // Here we only retry once
            console.log(`Retrying job ${id} in 1s!`);
            return 1000;
          }
        });

        let responses = [];
        
        if(config.methods[settings.type].directPath) {
            if(!settings.data) throw Error("Request type needs data");
            if(Array.isArray(settings.data)) {
                await Promise.all(settings.data.map(async singleData => await request(null, singleData)));
            } else {
                await request(null, settings.data);
            }
        } else {
            await request();
        }
        

        async function request(nextToken, pathData) {
            const res = await limiter.schedule(() =>makeRequest(settings, nextToken, pathData));
            //console.log("gotres: " + settings.type);
            if(config.methods[settings.type].insetData) {
                responses.push({
                    ...res.body,
                    [config.methods[settings.type].insetDataName]: pathData
                });
            } else {
                responses.push(res.body);
            }
            if(res.nextToken) await request(res.nextToken, pathData);
        }
        if(config.methods[settings.type].array) {
            let combined = [];
            responses.forEach(r => {
                combined = combined.concat(r[config.methods[settings.type].arrayKey]);
            });
            return combined;
        } else {
            return responses;
        }
    }
};